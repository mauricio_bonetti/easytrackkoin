package com.bon.handler;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.bon.service.EasyTrack;
import com.bon.util.JSONUtil;
import com.bon.util.ResponseUtil;
import com.bon.vo.Response;

/**
 * This class handles JSON-WPS requests in order to accomplish the shortest path
 * problem.
 * 
 * @author Mauricio Bonetti
 * 
 */
@Path("/easytrack")
public class Handler {
	/**
	 * This method is called when user reaches url: <server>/easytrack/getdistance. It
	 * handles a JSON-WSP request in order to find the distance between two places
	 * problem.
	 * 
	 * @param jsonRequest
	 * @return JSON-WPS response
	 */
	@GET
	@Path("/getdistance")
	@Produces(MediaType.APPLICATION_JSON)
	public String doService(@QueryParam("sourceId") int sourceId,
			@QueryParam("destinyId") int destinyId) {
		try {
			if (sourceId <= 0 || destinyId <= 0) {
				throw new RuntimeException("Invalid Request Parameters");
			}
			return getDistance(sourceId, destinyId);
		} catch (Exception e) {
			return errorResponse(e);
		}
	}
	
	/**
	 * This method is called when user reaches url: <server>/easytrack/findtwonearestplaces. It
	 * handles a JSON-WSP request in order to find the two nearest places
	 * 
	 * @param jsonRequest
	 * @return JSON-WPS response
	 */
	@GET
	@Path("/findtwonearestplaces")
	@Produces(MediaType.APPLICATION_JSON)
	public String doService() {
		try {
			return findTwoNearestPlaces();
		} catch (Exception e) {
			return errorResponse(e);
		}
	}

	/**
	 * This method calls service package in order to evaluate the distance and builds the webservice reponse
	 * @param sourceId
	 * @param destinyId
	 * @return getDistance method response
	 */
	private String getDistance(int sourceId, int destinyId) {
		EasyTrack service = EasyTrack.getInstance();
		double distance = service.getDistance(sourceId, destinyId);
		Response response = ResponseUtil.loadSuccessGetDistanceResponse(distance);
		return JSONUtil.toJson(response);
	}
	
	/**
	 * This method calls service package in order to find the two nearest places and builds the webservice reponse
	 * @param sourceId
	 * @return
	 */
	private String findTwoNearestPlaces() {
		EasyTrack service = EasyTrack.getInstance();
		List<Integer> twoNearestPlaces = service.findTwoNearestPlaces();
		Response response = ResponseUtil.loadSuccessFindTwoNearestPlacesResponse(twoNearestPlaces);
		return JSONUtil.toJson(response);
	}

	/**
	 * This method builds the error response in case of any Exception thrown.
	 * 
	 * @param request
	 * @param e
	 * @return
	 */
	private String errorResponse(Exception e) {
		return JSONUtil.toJson(ResponseUtil.loadDefaultErrorResponse(e));
	}
}

package com.bon.service;

import java.util.List;

import com.bon.util.ConnectionManager;
import com.bon.vo.Place;

/**
 * This is a service class that is responsible for calling easytrackcore in
 * order to accomplish the shortest path problem
 * 
 * @author Mauricio Bonetti
 * 
 */
public class EasyTrack {
	private static EasyTrack inst;

	private EasyTrack() {}

	/**
	 * Singleton!
	 */
	public static EasyTrack getInstance() {
		if (inst == null) {
			inst = new EasyTrack();
		}
		return inst;
	}

	/**
	 * This method calls Core package in order to evaluate the distance between two places
	 * @param sourceId
	 * @param destinyId
	 * @return distance in kilometers between source and destiny
	 */
	public double getDistance(int sourceId, int destinyId) {
		ConnectionManager m = null;
		try {
			m = new ConnectionManager();
			Place source = Core.findPlace(sourceId, m);
			Place destiny = Core.findPlace(destinyId, m);
			return Core.getDistance(source, destiny);
		} finally {
			m.closeConnection();
		}
	}
	
	/**
	 * This method calls service package in order to find the two nearest places
	 * @return a list containing the ids of two nearest places
	 */
	public List<Integer> findTwoNearestPlaces() {
		ConnectionManager m = null;
		try {
			m = new ConnectionManager();
			return Core.findTwoNearestPlaces(m);
		} finally {
			m.closeConnection();
		}
	}
	
	
}

package com.bon.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.bon.vo.Response;

/**
 * This class contains some utils in order to handle the webservice responses
 * 
 * @author Mauricio Bonetti
 * 
 */
public class ResponseUtil {
	/**
	 * Loads the default error response
	 * 
	 * @param e
	 * @return default error response
	 */
	public static Response loadDefaultErrorResponse(Exception e) {
		return loadErrorResponse(e, "null", "null");
	}

	/**
	 * Loads an error response
	 * 
	 * @param e
	 * @param errorMethod
	 * @param mirrorId
	 * @return error response
	 */
	private static Response loadErrorResponse(Exception e, String errorMethod,
			String mirrorId) {
		Response resp = new Response();
		resp.setType("jsonwsp/response");
		resp.setVersion("1.0");
		resp.setServicename("easytrackws");
		resp.setMethod(errorMethod);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", false);
		result.put("stack", ExceptionUtils.getStackTrace(e));
		resp.setResult(result);
		return resp;
	}

	/**
	 * Loads the successful getDistance response
	 * @param distance
	 * @return JSON-WSP succes response
	 */
	public static Response loadSuccessGetDistanceResponse(double distance) {
		Response resp = new Response();
		resp.setType("jsonwsp/response");
		resp.setVersion("1.0");
		resp.setServicename("easytrackws");
		resp.setMethod("getDistance");
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("distance", distance);
		resp.setResult(result);
		return resp;
	}

	/**
	 * Loads the successful findTwoNearestPlaces response
	 * @param distance
	 * @return JSON-WSP succes response
	 */
	public static Response loadSuccessFindTwoNearestPlacesResponse(
			List<Integer> twoNearestPlaces) {
		Response resp = new Response();
		resp.setType("jsonwsp/response");
		resp.setVersion("1.0");
		resp.setServicename("easytrackws");
		resp.setMethod("findTwoNearestPlaces");
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("places", twoNearestPlaces);
		resp.setResult(result);
		return resp;
	}
}

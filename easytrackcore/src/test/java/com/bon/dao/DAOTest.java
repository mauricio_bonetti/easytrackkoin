package com.bon.dao;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import com.bon.vo.Place;
import com.mysql.jdbc.PreparedStatement;

@RunWith(JUnit4.class)
public class DAOTest {
	@Test
	public void findPlaceTest() throws Exception {
		Connection mockConn = Mockito.mock(Connection.class);
		PreparedStatement mockStmt = Mockito.mock(PreparedStatement.class);
		ResultSet mockSet = Mockito.mock(ResultSet.class);
		when(mockConn.prepareStatement(anyString())).thenReturn(mockStmt);
		when(mockStmt.executeQuery()).thenReturn(mockSet);
		when(mockSet.next()).thenReturn(true);

		when(mockSet.getInt("ID")).thenReturn(1);
		when(mockSet.getString("NAME")).thenReturn("São Paulo");
		when(mockSet.getDouble("LATITUDE")).thenReturn(-23.5475000);
		when(mockSet.getDouble("LONGITUDE")).thenReturn(-46.6361100);

		Place expectedPlace = new Place(1, "São Paulo", -23.5475000,
				-46.6361100);
		Place actualPlace = DAO.findPlace(1, mockConn);
		Assert.assertEquals(expectedPlace, actualPlace);
	}

	@Test
	public void findEveryPlaceTest() throws Exception {
		Connection mockConn = Mockito.mock(Connection.class);
		PreparedStatement mockStmt = Mockito.mock(PreparedStatement.class);
		ResultSet mockSet = Mockito.mock(ResultSet.class);
		when(mockConn.prepareStatement(anyString())).thenReturn(mockStmt);
		when(mockStmt.executeQuery()).thenReturn(mockSet);
		when(mockSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);

		when(mockSet.getInt("ID")).thenReturn(1).thenReturn(2);
		when(mockSet.getString("NAME")).thenReturn("São Paulo").thenReturn("Guarulhos");
		when(mockSet.getDouble("LATITUDE")).thenReturn(-23.5475000).thenReturn(-23.4627800);
		when(mockSet.getDouble("LONGITUDE")).thenReturn(-46.6361100).thenReturn(-46.5333300);
		
		List<Place> expectedPlaces = new ArrayList<Place>();
		expectedPlaces.add(new Place(1, "São Paulo", -23.5475000,
				-46.6361100));
		expectedPlaces.add(new Place(2, "Guarulhos", -23.4627800, -46.5333300));
		List<Place> actualPlaces = DAO.findEveryPlace(mockConn);
		
		Assert.assertEquals(expectedPlaces, actualPlaces);
	}
}

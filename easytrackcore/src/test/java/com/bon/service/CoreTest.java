package com.bon.service;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import junitx.framework.ListAssert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.bon.dao.DAO;
import com.bon.util.ConnectionManager;
import com.bon.vo.Place;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DAO.class)
public class CoreTest extends TestCase {
	@Test
	public void getDistanceTest() {
		Place place1 = new Place(1, "São Paulo", -23.5475000, -46.6361100);
		Place place2 = new Place(2, "Guarulhos", -23.4627800, -46.5333300);
		double actualDistance = Core.getDistance(place1, place2);
		double expectedDistance = 14.09189816732726;
		assertEquals(expectedDistance, actualDistance);
	}

	@Test
	public void findTwoNearestPlacesTest() {
		PowerMockito.mockStatic(DAO.class);
		List<Place> places = new ArrayList<Place>();
		places.add(new Place(1, "São Paulo", -23.5475000, -46.6361100));
		places.add(new Place(2, "Guarulhos", -23.4627800, -46.5333300));
		places.add(new Place(3, "Osasco", -23.5325000, -46.7916700));
		places.add(new Place(4, "Sorocaba", -23.5016700, -47.4580600));
		places.add(new Place(5, "Limeira", -22.5647200, -47.4016700));
		places.add(new Place(6, "Campinas", -22.9055600, -47.0608300));
		places.add(new Place(7, "Jundiaí", -23.1863900, -46.8841700));
		places.add(new Place(8, "Braganca Paulista", -22.9519400, -46.5419400));
		places.add( new Place(9, "Atibaia", -23.1169400, -46.5502800));
		places.add(new Place(10, "Bauru", -22.3147200, -49.0605600));
		PowerMockito.when(DAO.findEveryPlace(Matchers.any(Connection.class))).thenReturn(places);
		ConnectionManager mockManager = Mockito.mock(ConnectionManager.class);
		List<Integer> actualTwoNearestPlaces = Core.findTwoNearestPlaces(mockManager);
		List<Integer> expectedTwoNearestPlaces = new ArrayList<Integer>();
		expectedTwoNearestPlaces.add(7);
		expectedTwoNearestPlaces.add(9);
		ListAssert.assertEquals(expectedTwoNearestPlaces, actualTwoNearestPlaces);
	}

}

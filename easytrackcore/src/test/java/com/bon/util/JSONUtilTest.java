package com.bon.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class JSONUtilTest {
	@Test
	public void toJson() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("key1", "value1");
		map.put("key2", "value2");
		String jsonActual = JSONUtil.toJson(map);
		Assert.assertTrue(StringUtils.contains(jsonActual, "\"key2\":\"value2\""));
		Assert.assertTrue(StringUtils.contains(jsonActual, "\"key1\":\"value1\""));
	}
}

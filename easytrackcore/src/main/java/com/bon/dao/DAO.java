package com.bon.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bon.vo.Place;

/**
 * This class is responsible for accessing data from database in order to
 * accomplish the shortest path problem
 * 
 * @author Mauricio Bonetti
 * 
 */
public class DAO {
	/**
	 * This method makes an attempt to close PreparedStatement and ResultSet
	 * resources
	 * 
	 * @param stmt
	 * @param set
	 */
	private static void closeResources(PreparedStatement stmt, ResultSet set) {
		try {
			if (set != null) {
				set.close();
			}
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * This method makes an attempt to find a place in database with a certain
	 * objectId
	 * 
	 * @param objectId
	 * @param conn
	 * @return A place object
	 */
	public static Place findPlace(int objectId, Connection conn) {
		PreparedStatement stmt = null;
		ResultSet set = null;
		try {
			stmt = conn
					.prepareStatement("SELECT * FROM TBL_PLACE WHERE ID = ?");
			stmt.setInt(1, objectId);
			set = stmt.executeQuery();
			if (set.next()) {
				return new Place(set.getInt("ID"), set.getString("NAME"),
						set.getDouble("LATITUDE"), set.getDouble("LONGITUDE"));
			}
			throw new RuntimeException("Place with id = " + objectId
					+ "not found");
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResources(stmt, set);
		}
	}

	/**
	 * This method makes an attempt to find every place registered in database
	 * @param conn
	 * @return List of Places
	 */
	public static List<Place> findEveryPlace(Connection conn) {
		PreparedStatement stmt = null;
		ResultSet set = null;
		try {
			stmt = conn.prepareStatement("SELECT * FROM TBL_PLACE");
			set = stmt.executeQuery();
			List<Place> places = new ArrayList<Place>();
			while (set.next()) {
				places.add(new Place(set.getInt("ID"), set.getString("NAME"),
						set.getDouble("LATITUDE"), set.getDouble("LONGITUDE")));
			}
			if (places.size() <= 0) {
				throw new RuntimeException("Empty database table");
			}
			return places;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			closeResources(stmt, set);
		}
	}
}

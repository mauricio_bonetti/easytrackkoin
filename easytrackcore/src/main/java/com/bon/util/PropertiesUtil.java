package com.bon.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

/**
 * This method provides some utilities for handling properties
 * 
 * @author Mauricio Bonetti
 * 
 */
public class PropertiesUtil {
	private static Properties props;

	/**
	 * This method makes an attempt to get a value from its key
	 * 
	 * @param key
	 * @return value
	 */
	public static String get(String key) {
		if (props == null) {
			loadProperties();
		}
		String value = props.getProperty(key);
		if (StringUtils.isBlank(value)) {
			throw new RuntimeException("Property no foun for key " + key);
		}
		return value;
	}

	/**
	 * This method loads the property file connection.properties
	 */
	private static void loadProperties() {
		InputStream is = PropertiesUtil.class
				.getResourceAsStream("/connection.properties");
		props = new Properties();
		try {
			props.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}

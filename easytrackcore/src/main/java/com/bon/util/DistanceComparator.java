package com.bon.util;

import java.util.Comparator;
import java.util.Map;
/**
 * This class is a comparator for a Map<PlaceID, Distance>
 * @author Mauricio Bonetti
 *
 */
public class DistanceComparator implements Comparator<Integer> {
    Map<Integer, Double> map;

    public DistanceComparator(Map<Integer, Double> map) {
        this.map = map;
    }

    /**
     * This method compares two distances
     */
    public int compare(Integer a, Integer b) {
        if (map.get(a) >= map.get(b)) {
            return -1;
        } else {
            return 1;
        }
    }
}
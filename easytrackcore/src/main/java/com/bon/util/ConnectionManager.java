package com.bon.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class is responsible for managing a database connection
 * 
 * @author Mauricio Bonetti
 * 
 */
public class ConnectionManager {
	/**
	 * This block makes a lookup for driver's resource
	 */
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private Connection conn;

	/**
	 * This method retrieves a database JDBC connection. It's credentials are loaded
	 * from connection.properties file
	 * 
	 * @return JDBC Connection
	 */
	public Connection getConnection() {
		try {
			if (conn == null || conn.isClosed()) {
				String host = PropertiesUtil.get("host");
				String port = PropertiesUtil.get("port");
				String database = PropertiesUtil.get("database");
				String username = PropertiesUtil.get("username");
				String password = PropertiesUtil.get("password");
				conn = DriverManager.getConnection("jdbc:mysql://" + host + ":"
						+ port + "/" + database, username, password);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return conn;
	}

	/**
	 * This method makes an attempt to close a JDBC connection
	 */
	public void closeConnection() {
		try {
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}

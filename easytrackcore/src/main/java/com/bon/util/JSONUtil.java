package com.bon.util;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * This class provides some utils in order to handle JSON
 * 
 * @author Mauricio Bonetti
 * 
 */
public class JSONUtil {
	/**
	 * This method takes an object and converts it into a JSON String
	 * 
	 * @param obj
	 * @param c
	 * @return
	 */
	public static <T extends Object> String toJson(T obj, Class c) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Object json = objectMapper.readValue(
					objectMapper.writeValueAsString(obj), c);
			return objectMapper.writeValueAsString(json);
		} catch (Exception e) {
			throw new RuntimeException(
					"Unable to convert object to Json string");
		}
	}

	/**
	 * This method takes an object and converts it into a JSON String
	 * 
	 * @param v
	 * @return
	 */
	public static <T extends Object> String toJson(T v) {
		return toJson(v, v.getClass());
	}
}

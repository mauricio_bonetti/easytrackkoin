package com.bon.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeMap;

import com.bon.dao.DAO;
import com.bon.util.ConnectionManager;
import com.bon.util.DistanceComparator;
import com.bon.vo.Place;

/**
 * This class is responsible for business evaluations for the shortest path
 * problem
 * 
 * @author Mauricio Bonetti
 * 
 */
public class Core extends ConnectionManager {

	/**
	 * This method is responsible for calling DAO findPlace
	 * 
	 * @param objectId
	 * @return Place object
	 */
	public static Place findPlace(int objectId, ConnectionManager m) {
		return DAO.findPlace(objectId, m.getConnection());
	}

	/**
	 * This method is responsible for finding the two nearest places
	 * 
	 * @return A list containing the two nearest places
	 */
	public static List<Integer> findTwoNearestPlaces(ConnectionManager m) {
		List<Place> places = DAO.findEveryPlace(m.getConnection());
		
		// Creates a map that key = objectId, value = sum of distance from every other point
		Map<Integer, Double> totalDistances = getTotalDistances(places);
		
		// Sorts the map based on distance from source
		DistanceComparator comparator = new DistanceComparator(totalDistances);
		TreeMap<Integer, Double> sortedDestinies = new TreeMap<Integer, Double>(
				comparator);
		sortedDestinies.putAll(totalDistances);

		// Inverts map order
		NavigableSet<Integer> set = sortedDestinies.descendingKeySet();
		ArrayList<Integer> list = new ArrayList<Integer>(set);

		// Get the first and second nearest
		ArrayList<Integer> twoNearestPlaces = new ArrayList<Integer>();
		twoNearestPlaces.add(list.get(0));
		twoNearestPlaces.add(list.get(1));

		return twoNearestPlaces;
	}
	
	/**
	 * This method creates a map that registers the sum of every of distances from every point.
	 * @param places
	 * @return Map<Iteger, Double> => key = placeId, value = sum of distances from every point
	 */
	private static Map<Integer, Double> getTotalDistances(List<Place> places) {
		Map<Integer, Double> totalDistances = new HashMap<Integer, Double>();
		for (Place source : places) {
			double totalDistance = 0.0;
			int sourceId = source.getObjectId();
			for (Place destiny : places) {
				int destinyId = destiny.getObjectId();
				if (sourceId != destinyId) {
					double distance = getDistance(source, destiny);
					totalDistance += distance;
				}
			}
			totalDistances.put(sourceId, totalDistance);
		}
		return totalDistances;
	}

	/**
	 * This method evaluates the distance between two points using Haversine
	 * formula.
	 * 
	 * @param source
	 * @param destiny
	 * @return Distance in kilometers
	 */
	public static double getDistance(Place source, Place destiny) {
		Double latDistance = deg2rad(destiny.getLatitude()
				- source.getLatitude());
		Double lonDistance = deg2rad(destiny.getLongitude()
				- source.getLongitude());
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(deg2rad(source.getLatitude()))
				* Math.cos(deg2rad(destiny.getLatitude()))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		int R = 6371; // Radius of the earth
		double distance = R * c;
		distance = Math.pow(distance, 2);
		return Math.sqrt(distance);
	}

	/**
	 * Transform degrees to radians
	 * 
	 * @param deg
	 * @return degrees as radians
	 */
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}
}

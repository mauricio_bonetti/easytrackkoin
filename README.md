Descrição
-----------

O EasyTrack é uma solução para o problema proposto no processo seletivo da Koin. Ele é composto por três 
módulos, a ver:


**EasyTrackCore**

Responsável por encontrar a distância entre dois pontos persistidos na base e obter os dois lugares mais próximos de todos os outros pontos. A distância é calculada à partir das coordenadas geográficas (latitude e longitude) utilizando-se da fórmula Haversine (http://en.wikipedia.org/wiki/Haversine_formula) para pontos geodésicos.

**EasyTrackWs**

Webservice com duas interfaces, "getdistance" e "findtwonearestplaces". Esse módulo é 
responsável por receber as requisições, validar e invocar os métodos do core.
  
**EasyTrackServer**

Módulo com um servidor web embarcado (Jetty). É responsável por subir um servidor na 
porta 8080 e por realizar o deploy do webservice (Jersey) no endereço 

```
#!java

localhost:8080\easytrack
```


QuickStart
------------------

Criar uma base de dados MySql e executar o seguinte script:

```
#!sql
CREATE TABLE IF NOT EXISTS TBL_PLACE (
  ID INT(11) NOT NULL AUTO_INCREMENT,
  NAME VARCHAR(45) NOT NULL,
  LATITUDE DOUBLE NOT NULL,
  LONGITUDE DOUBLE NOT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('São Paulo', -23.5475000, -46.6361100);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Guarulhos', -23.4627800, -46.5333300);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Osasco', -23.5325000, -46.7916700);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Sorocaba', -23.5016700, -47.4580600);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Limeira', -22.5647200, -47.4016700);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Campinas', -22.9055600, -47.0608300);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Jundiaí', -23.1863900, -46.8841700);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Braganca Paulista', -22.9519400, -46.5419400);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Atibaia', -23.1169400, -46.5502800);
INSERT INTO TBL_PLACE(NAME, LATITUDE, LONGITUDE) VALUES ('Bauru', -22.3147200, -49.0605600);
```

Com isso, você tem uma base populada com a massa de testes a ser utilizada pela aplicação. 
Realizar o checkout da branch master dos módulos:

```
#!java

https://mauricio_bonetti@bitbucket.org/mauricio_bonetti/easytrackkoin.git
```

Configurar o arquivo de propriedades da aplicação para apontar para a base criada (easytrackcore\src\main\resources\connection.properties), exemplo:

```
#!java
host=localhost
port=3306
username=user
password=password
database=easytrack
```

Executar a instalação dos módulos na sequência:
```
#!java
mvn install (em easytrackcore)
mvn install (em easytrackws)
mvn install (em easytrackserver)
```
Com isso, será gerado no diretório target de easytrackserver o jar easytrackserver-
1.0.jar e um diretório lib com as dependências do projeto (easytrackws, easytrackcore, jetty, jersey...).
No diretório target de easytrackserver executar o comando:
```
#!java
(Windows)
java -cp ".\*;lib\*" com.bon.server.ServerStarter
```
Com isso, temos o servidor rodando na porta 8080 com o serviço instalado. Em um 
cliente HTTP, chamar a interface "getdistance", conforme:
```
#!java

Url: http://localhost:8080/easytrack/getdistance?sourceId=1&destinyId=2
```
Resposta do serviço:

```
#!java

{
    "result": {
        "distance": 14.09189816732726
    },
    "servicename": "easytrackws",
    "method": "getDistance",
    "type": "jsonwsp/response",
    "version": "1.0"
}
```

Com isso, temos que a distância entre o ponto 1 (São Paulo) e o ponto 2 (Guarulhos) é de 14.09 km. Se quisermos saber quais os dois pontos mais próximos de todos os pontos cadastrados na base, ou seja, aqueles cuja soma de todas as distâncias são as menores, fazemos:
```
#!java

Url: http://localhost:8080/easytrack/findtwonearestplaces

```

Resposta do Serviço:

```
#!java

{
    "result": {
        "places": [
            7,
            9
        ]
    },
    "servicename": "easytrackws",
    "method": "findTwoNearestPlaces",
    "type": "jsonwsp/response",
    "version": "1.0"
}
```

Com isso, temos que as duas cidades mais próximas de todas são 7 (Jundiaí) e 9 (Atibaia). Conforme podemos verificar numa tabela elaborada à partir da execução do método "getdistance" para todos os pontos:

![distancias.png](https://bitbucket.org/repo/yonRLj/images/177350024-distancias.png)

Solução
------------------

Para esse problema, achei importante fornecer uma solução que fosse independente de 
elementos de infra estrutura externos (servidor de aplicação). 
Assim, optei por embarcar um servidor web (Jetty). Pensei tratar a solução por módulos afim de separar as responsabilidades. Assim, criei um módulo que trata as regras do negócio (easytrackcore), outro que trata as requisições do serviço e gera as resposta (easytrackws) e outro que sobe o servidor e faz o deploy do serviço (easytrackserver). Para isso, escolhi algumas tecnologias para facilitar o desenvolvimento. O servidor é o Jetty. O webservice está fazendo mapeamento de requisições através do Jersey. O modelo de requisição é um JSON no protocolo JSON-WSP. Fiz o parse do JSON da resposta com o Jackson. O calculo da distância está sendo feito por uma implementação do algoritmo Haversine.


Refatorações
------------------

Acredito que nessa solução existe espaço para melhorias. Falta um tratamento para o 
log das requisições e das operações da aplicação. O projeto poderia ter sido montado 
à partir de um módulo "pai" com o build sendo feito através de seu pom.xml, elimiando 
a necessidade de executar três builds. Acredito que muito esforço foi empenhado em mapeamento de dados, ou seja, burocracia que poderia ser melhor endereçada com alguma solução já existente para webservices JSON. E outras modificações que vocês, certamente, apontarão...